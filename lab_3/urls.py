from django.urls import path
from .views import index, friend_form

urlpatterns = [
    path('', index, name='index'),
    path('add/',friend_form)
    # TODO Add friends path using friend_list Views
]