from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import friendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def friend_form(request):
    if request.method == 'POST':
        form = friendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')

    else:
        form = friendForm()

    return render(request, 'lab3_form.html', {'form': form})
