from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .NoteForm import noteForm

def index(request):
    messages = Note.objects.all()
    response = {"Notes":messages}
    return render(request,"lab4_index.html",response)

@login_required(login_url="/admin/login/")
def note_form(request):
    if request.method == 'POST':
        form = noteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')

    else:
        form = noteForm()

    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    messages = Note.objects.all()
    response = {"Notes":messages}
    return render(request,"lab4_note_list.html",response)
