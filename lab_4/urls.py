from django.urls import path
from .views import index,note_form,note_list

urlpatterns = [
    path('', index, name='index'),
    path('add-note/',note_form),
    path('note-list/',note_list),
]