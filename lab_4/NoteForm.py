from django import forms
from lab_2.models import Note

class noteForm (forms.ModelForm):
    class Meta:
        model = Note
        fields=["To","From","Title","Message"]; 