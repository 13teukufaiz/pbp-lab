import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lab_7/pallete.dart';
import 'package:lab_7/widgets/widgets.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Form(
      key: _formKey,
      child: Stack(
        children: [
        const BackgroundImage(
          image: 'assets/login_register_bg.jpg',
        ),
        Scaffold(
          backgroundColor: Colors.transparent,

          body: Column(
            children: [
              const Flexible(
                child: Center(
                  child: Text(
                    'Reflekt.io',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 60,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: "contoh: Dummy@gmail.com",
                        labelText: "Email",
                        labelStyle: TextStyle(color: Colors.white),
                        icon: const Icon(Icons.attach_email),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)
                        ),  
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Email tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: "Masukkan Password",
                        labelText: "Password",
                        labelStyle: TextStyle(color: Colors.white),
                        icon: Icon(Icons.lock_outline,),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),  
                        ),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),

                  const SizedBox(
                    height: 25,
                  ),
                  
                  Container(
                    height: size.height * 0.08,
                    width: size.width * 0.8,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: kBlack,
                    ),
                    child: FlatButton(
                      onPressed: () {if (_formKey.currentState!.validate()) {}},
                      child: Text(
                        'Submit',
                        style: kBodyText.copyWith(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  
                  const SizedBox(
                    height: 35,
                  ),
                ],
              ),
              GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'CreateNewAccount'),
                child: Container(
                  child: const Text(
                    'Create New Account',
                    style: kBodyText,
                  ),
                  decoration: const BoxDecoration(
                      border:
                          Border(bottom: BorderSide(width: 1, color: kWhite))),
                ),
              ),
              const SizedBox(
                height: 35,
              ),
            ],
          ),
        )
      ],
      )
    );
  }
}