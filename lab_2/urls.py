from django.urls import path
from .views import index, json_lab2, xml_lab2

urlpatterns = [
    path('', index, name='index'),
    path('xml/',xml_lab2),
    path('json/',json_lab2)
]
