from django.db import models

# Create your models here.

class Note(models.Model):
    To = models.CharField(max_length=150)
    From = models.CharField(max_length=150)
    Title = models.CharField(max_length=150)
    Message = models.CharField(max_length=150)