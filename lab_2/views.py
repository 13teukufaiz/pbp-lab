from django.http import response
from django.shortcuts import render
from .models import Note
from django.core import serializers
from django.http.response import HttpResponse

# Create your views here.

def index(request):
    messages = Note.objects.all()
    response = {"Notes":messages}
    return render(request,"lab2.html",response)

def xml_lab2(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json_lab2(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")