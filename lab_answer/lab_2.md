## 1. **Perbedaan antara JSON dan XML :**
- JSON menyimpan data seperti dictionary dengan key dan value. Sedangkan XML menyimpan data seperti tree structure.
- JSON mendukung tipe data teks (String) dan angka (integer) serta direpresentasikan menggunakan array dan objek. Sedangkan XML tidak dapat langsung direpresentasikan menggunakan array tetapi mendukung banyak tipe data seperti angka, teks, gambar, dan grafik.
- JSON mudah dibaca dan dipahami sehingga membuat file terlihat lebih sendiri. Sedangkan XML filenya berukuran besar serta struktur tag dan filenya rumit untuk dipahami.

## 2. **Perbedaan antara HTML dan XML :**
- File HTML tersusun dari tag yang mengatur bagaimana data akan ditampilkan, tetapi tidak ada informasi mengenai isi dari data tersebut. Sedangkan didalam file XML, kandungan informasi berbentuk format yang terstruktur karena data dan tampilannya dibuat terpisah.
- HTML difokuskan pada penyajian data. Sedangkan XML berfokus pada transfer data.
- HTML Case Insensitive. Sedangkan XML Case Sensitive.

### *Referensi :*
- [Apa Perbedaan JSON Dan XML](https://www.monitorteknologi.com/perbedaan-json-dan-xml/)
- [Apa Saja Perbedaan XML dan HTML](https://blogs.masterweb.com/perbedaan-xml-dan-html/)